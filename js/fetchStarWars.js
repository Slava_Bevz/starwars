//----------------------------------------------------Задание------------------------------------------------------------------

// Получить список всех планет из серии фильмов Звездные войны, и вывести на экран список персонажей, 
// для которых эта планета - родной мир.

//---------------------------------------------Технические требования:--------------------------------------------------------

// Отправить AJAX запрос по адресу https://swapi.co/api/planets/ и получить список всех планет серии фильмов Звездные войны.
// Для каждой планеты получить с сервера список персонажей, для которых она является родным миром. 
// Список персонажей можно получить из свойства residents.
// Как только с сервера будет получена информация о планетах, сразу же вывести список всех планет на экран. 
// Необходимо указать имя планеты, климат, а также тип преобладающей местности (поля name, climate и terrain).
// Как только с сервера будет получена информация о персонажах, родившихся на какой-то планете, вывести эту 
//   информацию на страницу под названием планеты.
// Необходимо написать два варианта реализации в разных .js файлах. Один - с помощью fetch, другой - с помощью promise.

let borderLoader = document.querySelector('.borderLoader');

window.addEventListener('load', () => {
    borderLoader.classList.add('hide');
    setTimeout(() => {
        borderLoader.remove();
    }),600;
})

const   requestURL = 'https://swapi.dev/api/planets/',
        tBody = document.querySelector('tbody'),
        create = value => document.createElement(value);

async function starWars(){
    let     request = await fetch(requestURL),
            requestJson = await request.json(),
            planets = requestJson.results;

    const   tr = create('tr'),
            tdName = create('th'),
            tdClimate = create('th'),
            tdTerrain = create('th');

    tdName.innerText = 'Имя планеты:';
    tdClimate.innerText = 'Климат:';
    tdTerrain.innerText = 'Тип местности:';

    tBody.append(tr);
    tr.append(tdName, tdClimate, tdTerrain);

    planets.forEach(element => {
        let     trPlanet = create('tr'),
                tdPlanetName = create('td'),    
                tdPlanetClimate = create('td'),    
                tdPlanetTerrain = create('td'),
                planetName = element.name,
                planetClimate = element.climate,
                planetRettain = element.terrain,
                [...residentsArr] = element.residents;

        tdPlanetName.innerText = planetName;
        tdPlanetClimate.innerText = planetClimate;
        tdPlanetTerrain.innerText = planetRettain;
                        
        tBody.append(trPlanet);
        trPlanet.append(tdPlanetName, tdPlanetClimate, tdPlanetTerrain); 
        trPlanet.classList.add('trPlanet');

        residentsArr.forEach(e =>{
            async function residentRequest(){
                const   requestResident = await fetch(e),
                        requestResidentJson = await requestResident.json(),
                        trPlanetResidents = create('tr'),
                        planetResidents = create('td'),
                        residentsAttribute = document.createAttribute('colspan');
                        
                residentsAttribute.value = '3';
                planetResidents.setAttributeNode(residentsAttribute);
                planetResidents.innerText = requestResidentJson.name;  

                trPlanet.after(trPlanetResidents);
                trPlanetResidents.append(planetResidents);
                trPlanetResidents.classList.add('trResidents');
            }
            residentRequest();
        });
    });
}

starWars();


